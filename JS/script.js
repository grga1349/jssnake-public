// CANVAS
var a_canvas = document.getElementById("script_canvas");
var context = a_canvas.getContext("2d");

// UZMI KEY EVENT OD BROWSERA
window.addEventListener ("keydown", getKey , false);

// POCETNE GLOBALNE VARIJABLE
var dir=0;
var snake =[];
snake.push ( {x:100,y:100} );
var n=1;
var mouse = {x:200,y:200};
var game_over=false;
var score=0;
var free_key=true;

// KEY INPUT
function getKey(key)
{
    if (free_key)
    {
        if((key.keyCode == "38")&&(dir!='down'))
        dir='up';
        if((key.keyCode == "40")&&(dir!='up'))
        dir='down';
        if((key.keyCode == "37")&&(dir!='right'))
        dir='left';
        if((key.keyCode == "39")&&(dir!='left'))
        dir='right';
        free_key=false;
    }
}
// BUTTON INPUT
function snUp()
{
   if(dir!='down')
   dir='up';
   free_key=false;
}

function snDown()
{
   if(dir!='up')
   dir='down';
   free_key=false;
}

function snRight()
{
  if(dir!='left')
  dir='right';
  free_key=false;
}

function snLeft()
{
   if(dir!='right')
   dir='left';
   free_key=false;
}
var a1= document.getElementById("up");
a1.addEventListener ("click", snUp , false);
var a2= document.getElementById("down");
a2.addEventListener ("click", snDown , false);
var a3= document.getElementById("left");
a3.addEventListener ("click", snLeft , false);
var a4= document.getElementById("right");
a4.addEventListener ("click", snRight , false);

// KAD IZIDE MISA
function snakeEat()
{
    if((snake[0].y==mouse.y)&&(snake[0].x==mouse.x))
    {
        snake.push({x:100,y:100});
        n++;
        mouse.x=Math.floor((Math.random()*20))*20;
        mouse.y=Math.floor((Math.random()*20))*20;
        score+=10;
        console.log(n);
    }
    if(n>1)
    for (i=n; i>1; i--)
    {

        snake[i-1].y=snake[i-2].y;
        snake[i-1].x=snake[i-2].x;
    }
}

// CRTAJ NA EKRAN
function snakeDraw()
{
    context.clearRect(0,0,400,400);
    // ekran
    context.fillStyle = "rgba(0,0,0,1)";
    context.beginPath();
    context.fillRect (0,0,400,400);
    context.closePath();
    // mis
    context.fillStyle = "yellow";
    context.beginPath();
    context.fillRect (mouse.x,mouse.y,20,20);
    context.closePath();
    // zmija
    for(i=0;i<n;i++)
    {
        console.log('crtam dio ',i);
        console.log(snake[i].x);
        console.log(snake[i].y);
        context.fillStyle = "yellow";
        context.beginPath();
        context.fillRect (snake[i].x,snake[i].y,20,20);
        context.closePath();
    }
    document.getElementById("score").innerHTML ='SCORE: '+score;
}

// POSTAVI SMJER
function snakeMove()
{
    if (dir=='up')
    snake[0].y=snake[0].y-20;
    if (dir=='down')
    snake[0].y=snake[0].y+20;
    if (dir=='left')
    snake[0].x=snake[0].x-20;
    if (dir=='right')
    snake[0].x=snake[0].x+20;
    if (snake[0].x>380)
    snake[0].x=0;
    if (snake[0].x<0)
    snake[0].x=380;
    if (snake[0].y>380)
    snake[0].y=0;
    if (snake[0].y<0)
    snake[0].y=380;
    free_key=true;
}

// JELI IGRA GOTOVA
function isGameOver()
{
    if(n>1)
        for(i=1;i<n;i++)
            if((snake[0].y==snake[i].y)&&(snake[0].x==snake[i].x))
                game_over =true;
}

// GLAVNA FUNKCIJA
function main()
{
    if (!game_over)
    {
        snakeEat();
        snakeMove();
    }
    snakeDraw();
    isGameOver();
}

// GLAVNI LOOP
setInterval (function(){main();},100);


